use actix_web::{HttpServer,get, App,Responder, HttpResponse};
use serde::{Serialize, Deserialize};
use std::error::Error;
use std::io::BufReader;
use std::fs::File;
use std::path::Path;

// #[derive(Serialize)]
// pub struct JsonResp {
//     name: String,
//     age: i32,
// }

#[derive(Serialize, Deserialize,Debug)]
pub struct JsonResp {
    count: usize,
    recipies: Vec<Recipie>,
    error: String
}

#[derive(Serialize, Deserialize,Debug)]
pub struct Ingredients {
    quantity: String,
    name: String,
    r#type: String,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Recipie {
    name: String,
    description: String,
    ingredients: Vec<Ingredients>,
    steps: Vec<String>,
    timers: Vec<i32>,
    #[serde(rename="imageURL",default)]
    
    image_url:String,
    #[serde(rename="originalURL", default)]
    original_url:String,
}
fn read_json<P: AsRef<Path>>(path: P) -> Result<Vec<Recipie>,Box<dyn Error>>{
    let file = File::open(path)?;
    let buff = BufReader::new(file);

    let r = serde_json::from_reader(buff)?;
    // println!("{:#?}",r);
    Ok(r)
}

#[get("/")]
async fn recipies() -> impl Responder {
    let r = read_json("recipes.json");
    // println!("{:#?}",r);
    // HttpResponse::Ok().json()
    match r {
        Ok(recipie) => {
            HttpResponse::Ok().json(
                JsonResp {
                    count: recipie.len(),
                    recipies: recipie,
                    error: "".to_string(),
                }
                )
        }
        _ => HttpResponse::Ok().json(
            JsonResp {
                count: 0,
                recipies: Vec::new(),
                error: "Something went wrong".to_string(),
            }
        )
    }
    
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().service(recipies)
    }
    ).bind("127.0.0.1:8080")?
    .run()
    .await
}