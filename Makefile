COLOR ?= always # Valid COLOR options: {always, auto, never}
CARGO = cargo --color $(COLOR)
TARGET = target
DEBUG = $(TARGET)/debug
RELEASE = $(TARGET)/release
BUILDDIR ?= build
NAME ?= tastyrecipes

all: build

bench:
	@$(CARGO) bench

build:
	@$(CARGO) build
	cp $(DEBUG)/$(NAME) $(BUILDDIR)

check:
	@$(CARGO) check

clean:
	@$(CARGO) clean

doc:
	@$(CARGO) doc

test: build
	@$(CARGO) test

update:
	@$(CARGO) update

release:
	@$(CARGO) build --release
	cp $(RELEASE)/$(NAME) $(BUILDDIR)
